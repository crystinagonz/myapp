import "./App.css";
import { ProductDetail } from "./components/ProductDetail";
/*import { Login } from "./components/Home/Login";*/
import NavBanner from "./components/NavBanner";
import BtnRegister from "./components/BtnRegister";
import "tailwindcss/tailwind.css";

function App() {
  return (
    <>
      <NavBanner />
      <BtnRegister />
      <ProductDetail />
    </>
  );
}

export default App;
