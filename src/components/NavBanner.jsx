const NavBanner = () => {
  return (
    <div className="flex items-center justify-between p-3 mt-0 lg:px-8">
      <img
        className="h-8 w-auto"
        src="../assets/archlinux.svg"
        alt="Icon Arch Linux"
      />
      <h1 className="text-3xl font-bold text-slate-200 fl p-6">MyApp</h1>
    </div>
  );
};
export default NavBanner;
