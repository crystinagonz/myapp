import { useState } from "react";
const BtnRegister = () => {
  const [textoBoton, setTextoBoton] = useState("VER animación");

  const handleButtonClick = () => {
    setTextoBoton("¡Botón clickeado!");
  };

  return (
    <button
      className="inline-flex items-center justify-center whitespace-nowrap rounded-md text-m font-bold ring-offset-background focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 text-primary-foreground hover:bg-primary/90 h-10 px-4 py-2 bg-purple-500 hover:bg-gradient-to-r hover:from-purple-500 hover:to-green-500 transition-all duration-500 ease-in-out transform hover:scale-110 animate-pulse mb-5"
      onClick={handleButtonClick}
    >
      {textoBoton}
    </button>
  );
};
export default BtnRegister;
