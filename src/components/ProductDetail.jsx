export const ProductDetail = () => {
  const objeto = {
    nombre: "Theremin",
    descripcion: "Instrumento musical electrónico construido con Arduino",
    precio: 0.0,
    sku: 1001,
    stock: 1,
  };
  return (
    <section className="w-[90%] max-w-md mx-auto bg-gray-700 md:flex md:rounded-2xl md:max-w-4xl">
      <article className="p-2 text-white text-center md:rounded-2xl md:w-1/2 md:grid md:content-center">
        <div className="bg-gray-900 rounded-b-2xl aspect-square grid content-center">
          <h2 className="text-5xl font-bold mb-2 md:text-7xl">
            {objeto.nombre}
          </h2>
          <img
            className="p-2 h-44 w-auto justify-self-center align-middle"
            src="/src/assets/theremin.webp"
            alt="Imagen del Theremin"
          />
        </div>
      </article>
      <article className="p-10 grid gap-3 md:w-1/2 text-white md:content-center">
        <p className="text-lg">{objeto.descripcion}</p>
        <p className="text-lg underline">Precio: ${objeto.precio.toFixed(2)}</p>
        <p className="text-lg">SKU: {objeto.sku}</p>
        <p className="text-lg inline bg-gray-900 rounded-b-2xl items-center">
          Cantidad Disponible: {objeto.stock}
        </p>
      </article>
    </section>
  );
};
