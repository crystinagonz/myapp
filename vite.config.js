import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";

export default defineConfig({
  plugins: [react()],
  preview: {
    host: true,
    port: 8080,
  },
  build: {
    outDir: "dist",
  },
  test: {
    enviroment: "jsdom",
  },
  css: {
    postcss: "./postcss.config.js",
  },
});
