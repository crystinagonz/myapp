import { render, screen } from "@testing-library/react";
import Login from "../../src/components/Home/Login";

test("renders login component", () => {
  render(<Login />);
  const loginElement = screen.getByText(/Login/i);
  expect(loginElement).toBeInTheDocument();
});
