# React + Vite + Tailwind

Ejercicios para el curso de Desarrollador Web con React JS de la UTN.

## Tecnologías Utilizadas

- ![React JS](https://img.shields.io/badge/React%20JS-%2361DAFB.svg?style=for-the-badge&logo=react&logoColor=white) - Biblioteca de JavaScript para construir interfaces de usuario.
- ![Vite](https://img.shields.io/badge/Vite-%232C3A42.svg?style=for-the-badge&logo=vite&logoColor=%23FF6600) - Herramienta de construcción rápida para proyectos web modernos.
- ![Tailwind CSS](https://img.shields.io/badge/Tailwind%20CSS-%2338B2AC.svg?style=for-the-badge&logo=tailwind-css&logoColor=white) - Framework de CSS utilizable con JavaScript.
- ![Docker](https://img.shields.io/badge/-Docker-black?style=flat-square&logo=docker)

### Descripción

Este proyecto contiene ejercicios desarrollados como parte del curso de Desarrollador Web con React JS de la UTN. La combinación de React, Vite y Tailwind ofrece un entorno moderno y eficiente para el desarrollo web.

Utilizo este repo para las primera pruebas con Docker.

<img src="/src/assets/VENpak.png" alt="Emoji with Docker" width="150" style="float:right;">
